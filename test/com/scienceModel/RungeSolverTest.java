package com.scienceModel;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Varamadon on 20.06.2018.
 */
public class RungeSolverTest {
    private RungeSolver rungeSolver;

    @Before
    public void setUp() {
        rungeSolver = new RungeSolver();
    }


    private List<Double> linearFunc(List<Double> y){
        if (y.size()!=1){
            return null;
        }
        double result = y.get(0) * Math.cos(y.get(0));
        List<Double> listResult = new ArrayList<>(1);
        listResult.add(result);
        return listResult;
    }

    @Test
    public void solveDiffure_linearEq(){
        Double y0 = 1.0;
        List<Double> list0 = new ArrayList<>(1);
        list0.add(y0);
        List<Double> listResult = rungeSolver.solve(new RungeSolver.RungeFunction() {
            @Override
            public List<Double> run(List<Double> y) {
                return linearFunc(y);
            }
        },list0,5);
        double result = listResult.get(0);
        assertEquals(1.10406,result,0.0001);
    }

}
