package com.scienceModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Varamadon on 19.06.2018.
 */
public class Model {
    private double potential;
    private double[] susceptible, infectious;
    private double resistant;
    private double[][] kIJ;
    private double[][] omega;
    private double[] bI;
    private double[] mu;
    private double h;
    private double b;
    private List<Double> i1Graph;
    private List<Double> rGraph;


    public Model(double[] susceptible, double[] infectious, double resistant, double[][] omega, double[] mu, double h, double b) {
        this.susceptible = susceptible;
        this.infectious = infectious;
        this.resistant = resistant;
        this.omega = omega;
        this.mu = mu;
        this.h = h;
        this.b = b;
        i1Graph = new ArrayList<>();
        rGraph = new ArrayList<>();
    }

    public List<Double> getI1Graph() {
        return i1Graph;
    }

    public List<Double> getrGraph() {
        return rGraph;
    }

    private double bSum() {
        double result = 0;
        for (double number : bI
                ) {
            result = result + number;
        }
        return result;
    }

    private double lambdI(int i) {
        double result = 0;
        for (int j = 0; j < susceptible.length; j++) {
            result += omega[i][j] * resistant * infectious[j] * susceptible[i];
        }
        return result;
    }

    private List<Double> pack() {
        List<Double> result = new ArrayList<>();
        result.add(resistant);
        for (double number : susceptible
                ) {
            result.add(number);
        }
        for (double number : infectious
                ) {
            result.add(number);
        }
        return result;
    }

    private double[] dSdt(double[] s, double[] i) {
        double[] result = new double[s.length];
        for (int ii = 0; ii < s.length; ii++) {
            result[ii] = -1 * lambdI(ii);
        }
        return result;
    }

    private double[] dIdt(double[] s, double[] i) {
        double[] result = new double[s.length];
        for (int ii = 0; ii < s.length; ii++) {
            result[ii] = lambdI(ii);
        }
        return result;
    }

    private double dPdt(double b, double[] i) {
        return 0;
    }

    private double dRdt(double[] mu) {
        double result = 0;
        for (int i = 0; i < mu.length; i++) {
            result += mu[i] * lambdI(i);
        }
        return result - b;
    }

    public List<Double> allDt(List<Double> arguments, double[] mu) {
        List<Double> result = new ArrayList<>();                    // сначала resistant,  затем список s, затем список i
        int n = susceptible.length;
        double r = arguments.get(0);
        double[] s = new double[n];
        for (int ii = 0; ii < n; ii++) {
            s[ii] = arguments.get(ii + 1);
        }
        double[] i = new double[n];
        for (int ii = 0; ii < n; ii++) {
            i[ii] = arguments.get(ii + 1 + n);
        }
        result.add(dRdt(mu));
        double[] sResult = dSdt(s, i);
        double[] iResult = dIdt(s, i);
        for (double number : sResult
                ) {
            result.add(number);
        }
        for (double number : iResult
                ) {
            result.add(number);
        }

        return result;
    }

    public void simulate() {
        i1Graph.add(infectious[2]);
        rGraph.add(resistant);
        int n = susceptible.length;
        List<Double> nextArguments;
        RungeSolver solver = new RungeSolver();
        nextArguments = solver.solve(new RungeSolver.RungeFunction() {
            @Override
            public List<Double> run(List<Double> y) {
                return allDt(y, mu);
            }
        }, pack(), h);
        resistant = nextArguments.get(0);
        for (int ii = 0; ii < n; ii++) {
            susceptible[ii] = nextArguments.get(ii + 1);
        }
        for (int ii = 0; ii < n; ii++) {
            infectious[ii] = nextArguments.get(ii + 1 + n);
        }
    }

}
