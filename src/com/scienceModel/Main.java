package com.scienceModel;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

import static javafx.application.Application.launch;

/**
 * Created by Varamadon on 20.06.2018.
 */
public class Main extends Application {

    private static final double W = 1000;
    private static final double H = 500;
    private static final double DS = 40;
    private static final double DP = 6;
    private static final double h = 0.02;
    private static final double stretchCoeff = H/2 - 10;

    private static final Canvas canvas = new Canvas(W, H);


    @Override
    public void start(Stage stage) {
        stage.setScene(new Scene(new Group(canvas)));
        stage.show();
        show();
    }

    private void show() {
        double[] s = {0.9,0.8,1};
        double[] i = {0.1,0.2,0};
        double r = 0.05;
        double[] mu = {0.5,0.5,0.5};
        double[][] omega = {
                {0.73,0.22,0.05},
                {0.04,0.87,0.09},
                {0.04,0.46,0.5}
        };
        double b = 0.01008;
        Model model = new Model(s,i,r,omega,mu,h,b);
        for (int ii=0;ii<3000;ii++){
            model.simulate();
        }
        List<Double> graf1 = model.getI1Graph();
        List<Double> scaledGraf1 = new ArrayList<>(graf1.size());
        for (double number: graf1
             ) {
            scaledGraf1.add(number * H);
        }
        List<Double> graf2 = model.getrGraph();
        List<Double> scaledGraf2 = new ArrayList<>(graf2.size());
        for (double number: graf2
                ) {
            scaledGraf2.add(number * H);
        }
        draw(scaledGraf1,Color.RED);
        draw(scaledGraf2, Color.GREEN);

    }

    private static List<Double> linearFunc(List<Double> y) {
        if (y.size() != 1) {
            return null;
        }
        double yResult = y.get(0);
        double result = 5;
        List<Double> listResult = new ArrayList<>(1);
        listResult.add(result);
        return listResult;
    }

    private static List<Double> cosList() {
        double k = 0;
        List<Double> result = new ArrayList<>();
        for (int i = 0; i < 2000; i++) {
            result.add(Math.cos(k)*stretchCoeff + stretchCoeff + 10);
            k += h;
        }
        return result;
    }

    private static List<Double> diffureList(){
        RungeSolver rungeSolver;
        rungeSolver = new RungeSolver();
        Double y0 = 1.0;
        Double x0 = 0.0;
        List<Double> finalResult = new ArrayList<>();
        for (int i=0;i<1000;i++){
            List<Double> list0 = new ArrayList<>(1);
            list0.add(y0);
            List<Double> listResult = rungeSolver.solve(new RungeSolver.RungeFunction() {
                @Override
                public List<Double> run(List<Double> y) {
                    return linearFunc(y);
                }
            }, list0, h);
            double result = listResult.get(0);
            finalResult.add(result);
            y0 = result;
        }
        return finalResult;
    }

    private static void draw(List<Double> that, Color color) {
        double space = W/that.size();
        double k = 0;
        final GraphicsContext graphicsContext = canvas.getGraphicsContext2D();

//        graphicsContext.setFill(Color.WHITE);
//        graphicsContext.fillRect(0, 0, W, H);

        for (double number : that
                ) {
            double x = k;
            double y = H - number;
            graphicsContext.setFill(color);
            graphicsContext.fillOval(x, y, 1, 1);
            k += space;
        }
    }

    public static void main(String[] args) {

        launch(args);

//        RungeSolver rungeSolver;
//        rungeSolver = new RungeSolver();
//        Double y0 = 1.0;
//        Double x0 = 0.0;
//        List<Double> list0 = new ArrayList<>(1);
//        list0.add(y0);
//        List<Double> listResult = rungeSolver.solve(new RungeSolver.RungeFunction() {
//            @Override
//            public List<Double> run(List<Double> y) {
//                return linearFunc(y);
//            }
//        }, list0, h);
//        double result = listResult.get(0);
//        System.out.println(result);


    }
}
