package com.scienceModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Varamadon on 19.06.2018.
 */
public class RungeSolver {

    public List<Double> solve(RungeFunction function, List<Double> y, double h) {

        List<Double> argument = new ArrayList<>();
        List<Double> result = new ArrayList<>();
        List<Double> k1 = function.run(y);
        for (int i = 0; i < y.size(); i++) {
            argument.add(y.get(i) + (h / 2) * k1.get(i));
        }
        List<Double> k2 = function.run(argument);
        argument.clear();
        for (int i = 0; i < y.size(); i++) {
            argument.add(y.get(i) + (h / 2) * k2.get(i));
        }
        List<Double> k3 = function.run(argument);
        argument.clear();
        for (int i = 0; i < y.size(); i++) {
            argument.add(y.get(i) + (h) * k3.get(i));
        }
        List<Double> k4 = function.run(argument);
        argument.clear();
        for (int i = 0; i < y.size(); i++) {
            result.add(y.get(i) + (h / 6) * (k1.get(i) + 2 * k2.get(i) + 2 * k3.get(i) + k4.get(i)));
        }
        return result;
    }

    public interface RungeFunction {
        List<Double> run(List<Double> y);
    }
}
